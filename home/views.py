from django.shortcuts import render, redirect
from pprint import pprint

from projects.models import Project
from django.http.response import HttpResponseRedirect

def home(request):
    

    if (request.user.is_authenticated):
        
        user = request.user
        user_projects = Project.objects.filter(user = user.profile)
        customer_projects = list(Project.objects.filter(participants__id=user.id).order_by().distinct())
        for team in user.profile.teams.all():
            customer_projects.append(team.task.project)
        cd = {}
        for customer_project in customer_projects:
            cd[customer_project.id] = customer_project

        customer_projects = cd.values()
        given_offers_projects = Project.objects.filter(pk__in=get_given_offer_projects(user)).distinct()

        deadline_customer_projects = sort_projects("deadline", customer_projects)

        '''if list(request.GET.keys())[0] == "own_sort_projects": # check if request from own projects
            user_projects = sort_projects(request.GET.get("own_sort_projects"), user_projects)  # return in order clicked
        elif list(request.GET.keys())[0] == "customer_sort_projects":
            customer_projects = sort_projects(request.GET.get("customer_sort_projects"), customer_projects)
        elif list(request.GET.keys())[0] == "finsished_sort_projects":
            pass # where?'''

        if request.GET.get('own_sort_projects') == 'deadline':
            print("request", request.GET.get("sort_deadline"))
            user_projects = sort_projects(user_projects)

        return render(
        request,
        'index.html',
        {
            'user_projects': user_projects,
            'customer_projects': customer_projects,
            'given_offers_projects': given_offers_projects,
            "deadline_customer_projects": deadline_customer_projects,
            
        })
    

    else:
        return redirect('projects')

def get_given_offer_projects(user):
    project_ids = set()

    for taskoffer in user.profile.taskoffer_set.all():
        project_ids.add(taskoffer.task.project.id)

    return project_ids



def sort_projects(key, projects):
    '''returns projects in order either by creation or deadline depending on the value of key.
    '''
    if key == "creation":
        return projects

    sorted_list = sorted(list(projects), key=lambda x: (x.deadline is None, x.deadline))
    print( "sorted list: ", sorted_list)
    return sorted_list