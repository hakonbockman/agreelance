$('#project-toggle a[data-toggle="list"]').on('shown.bs.tab', function (e) {
  $('#'+e.relatedTarget.id).removeClass('active') // previous active tab
}) 

$('#sort-project-toggle a[data-toggle="list"]').on('shown.bs.tab', function (e) {
  $('#'+e.relatedTarget.id).removeClass('active') // previous active tab
  //$('#'+e.relatedTarget.id).removeClass('show')
})

$('#deadline-sort').on("click", function(e) {
  document.getElementById("nav-tabContent").style.display = "none"
  document.getElementById("nav-sorted-tabContent").style.display = "block"
})

$('#creation-sort').on("click", function(e) {
  document.getElementById("nav-sorted-tabContent").style.display = "none"
  document.getElementById("nav-tabContent").style.display = "block"
})

$('.printMe').click(function(){
  $("#outprint").print();
});